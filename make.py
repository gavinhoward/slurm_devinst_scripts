#!/usr/bin/env python3

import argparse
import itertools as it
import logging
import os
import sys
from contextlib import contextmanager
from pathlib import Path
from subprocess import DEVNULL

import trio


src = Path('src')
cwd = Path('.')
plugin_dirs = [src/'plugins'/x for x in [
    'mpi',
    'job_submit',
    'task',
    'jobcomp']]
RECURSE = set([cwd, src, cwd/'contribs', src/'plugins', *plugin_dirs])
#RECURSE = set([src, src/'plugins'])
SKIP = set([cwd/'etc', cwd/'testsuite', cwd/'auxdir'])
made = set()


async def run_subproc(prog, *args, shell=True, **kwargs):
    global output
    # cwd = str(cwd.resolve())
    await trio.run_process(prog, **output, shell=shell, **kwargs)


async def make(path, nursery=None, install=True):
    if not nursery:
        async with trio.open_nursery() as nursery:
            await make(path, nursery=nursery)
        return

    if path in made:
        log.log(debug2, f"already done: {path}")
        return
    if path in RECURSE:
        for subpath in filter(Path.is_dir, path.iterdir()):
            nursery.start_soon(make, subpath)
    elif (path/'Makefile').exists():
        made.add(path)
        log.info(f"make: {path}")
        make_cmd = "make -j install" if install else "make -j"
        await run_subproc(make_cmd, cwd=path)


async def configure(args):
    # Because this operation requires user input, output is not optional
    # Is that reasonable?
    print('Configuring build dir')
    print('Build dir: {}'.format(args.build_dir))
    print(args.configure)
    while True:
        print('Confirm Y/n')
        resp = input().lower()
        if resp in ('', 'y'):
            break
        elif resp == 'n':
            return False
    print('configuring')
    await run_subproc(args.configure)
    return True


async def clean():
    log.info('cleaning build directory')
    cmd = 'make -j clean'
    log.info('\t{}'.format(cmd))
    await run_subproc(cmd)


async def recheck():
    log.info("running configure recheck")
    cmd = "./config.status --recheck"
    log.info(f"\t{cmd}")
    await run_subproc(cmd)


async def reconfig():
    log.info('running reconfigure')
    cmd = './config.status'
    log.info('\t{}'.format(cmd))
    await run_subproc(cmd)


async def main(args):
    if args.init:
        await configure(args)
    else:
        if args.recheck:
            await recheck()
        if args.reconfig:
            await reconfig()
    if args.clean:
        await clean()

    async with trio.open_nursery() as nursery:
        nursery.start_soon(make, src/'database')
        nursery.start_soon(make, src/'bcast')
        await make(src/'common')
        await make(src/'api')
        if (src/'db_api').exists():
            await make(src/'db_api')

    async with trio.open_nursery() as nursery:
        nursery.start_soon(make, src)
        for extra in args.extra:
            nursery.start_soon(make, cwd/extra)
        if args.docs:
            nursery.start_soon(make, cwd/'doc')
        if args.contribs:
            nursery.start_soon(make, cwd/'contribs')


@contextmanager
def cd(path):
    prev_dir = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_dir)


log_level = {
    'critical': 0,
    'error': 1,
    'info': 2,
    'debug': 3,
}

debug2 = 60
logging.addLevelName(debug2, 'debug2')  # independent logging level
log_map = (
    logging.CRITICAL,
    logging.ERROR,
    logging.INFO,
    logging.DEBUG,
)

OPTIONS = (
    ('build_dir',
     dict(action='store', nargs='?', type=lambda s: Path(s), default="",
          help="specify the build directory. Current working directory is used by default. If current dir is not a slurm build dir and SLURM_PREFIX env var exists, uses that instead")),
    ('--init', '-i',
     dict(dest='init', action='store_true', default=[],
          help="initialize build dir using configure flags in CONFIG_FLAGS. Also enables --serial. Requires variables CONFIG_FLAGS and (SLURM_SRC or INST_DIR) to be defined")),
    ('--serial', '-s',
     dict(dest='serial', action='store_true',
          help="Disable parallel build, just run make from the top")),
    ('--no-install',
     dict(dest='install', action='store_false',
          help="build only, do not install")),
    ('--with-clean', '-c',
     dict(dest='clean', action='store_true',
          help="run make clean before make")),
    ('--with-recheck', '-r',
     dict(dest='recheck', action='count',
          help="run config.status --recheck before make. Pass twice to imply --with-reconfig")),
    ('--with-reconfig',
     dict(dest='reconfig', action='store_true',
          help="run config.status before make")),
    ('--with-all', '-a',
     dict(dest='all', action='store_true',
          help="make docs and contribs")),
    ('--with-docs',
     dict(dest='docs', action='store_true',
          help="also make docs")),
    ('--with-contribs',
     dict(dest='contribs', action='store_true',
          help="also make contribs")),
    ('--with-extra',
     dict(dest='extra', action='append', default=[],
          help="specify extra subdirs to build, such as contribs/pmi2. Use multiple times or pass comma-separated list")),
    ('--level',
     dict(dest='level', action='store', default='error', choices=log_level.keys(),
          help="Set log level. Default is error, critical disables make stderr")),
    ('-v', '--verbose',
     dict(dest='verbose', action='count', default=0,
          help="Increase log level by 1")),
    ('-q', '--quiet',
     dict(dest='quiet', action='count', default=0,
          help="Reduce log level by 1")),
    ('--stdout',
     dict(dest='stdout', action='store_true',
          help="output full make stdout like a reckless child")),
    ('--no-stderr',
     dict(dest='stderr', action='store_false',
          help="Force disable make stderr")),
    ('--debug2',
     dict(dest='debug2', action='store_true',
          help=argparse.SUPPRESS)),
)

parser = argparse.ArgumentParser(
    description="Fast make slurm. Does not make docs or contribs by default.")
for x in OPTIONS:
    parser.add_argument(*x[:-1], **x[-1])
args = parser.parse_args()

log = logging.getLogger(__name__)
args.verbosity = log_level[args.level]
args.verbosity = max(0, min(3, args.verbosity + args.verbose - args.quiet))

log.setLevel(log_map[args.verbosity])
out = logging.StreamHandler(sys.stdout)
out.addFilter(lambda r: r.levelno <= logging.INFO)
err = logging.StreamHandler(sys.stderr)
err.addFilter(lambda r:  logging.INFO < r.levelno <= logging.CRITICAL)
if args.debug2:
    dbg2 = logging.StreamHandler(sys.stdout)
    dbg2.addFilter(lambda r: r.levelno == debug2)
    log.addHandler(dbg2)
log.addHandler(out)
log.addHandler(err)

# For subprocess output, block stdout unless log level is DEBUG
output = dict(stdout=None, stderr=None)
if not args.stdout:
    output['stdout'] = DEVNULL
if args.stderr:
    if log.level > logging.ERROR:
        output['stderr'] = DEVNULL
else:
    output['stderr'] = DEVNULL

if args.all:
    args.docs, args.contribs = True, True

# --with-recheck specified more than once turns on reconfig, allowing -rr
if args.recheck:
    if args.recheck > 1:
        args.reconfig = True
    args.recheck = True

if args.init:
    args.serial = True
    args.recheck = False
    args.reconfig = False
    args.clean = False
    if 'CONFIG_FLAGS' in os.environ:
        config_flags = os.environ['CONFIG_FLAGS']
    else:
        log.error("--init flag passed, but CONFIG_FLAGS env var not found")
        exit()
    if 'SLURM_SRC' in os.environ:
        srcdir = os.environ['SLURM_SRC']
    else:
        if 'INST_DIR' in os.environ:
            srcdir = Path(os.environ['INST_DIR'])/'source'
        else:
            log.error("--init flag passed, but SLURM_SRC or INST_DIR env var not found")
            exit()
    args.configure = f'{srcdir}/configure {config_flags}'

if not (args.build_dir/'slurm/slurm.h').exists():
    if 'SLURM_PREFIX' in os.environ:
        args.build_dir = Path(os.environ['SLURM_PREFIX'])/'build'
        log.error("Using SLURM_PREFIX: {}".format(os.environ['SLURM_PREFIX']))
    if not (args.build_dir/'slurm/slurm.h').exists() and not args.init:
        log.error("This does not appear to be a Slurm build directory. Make sure to run configure first.")
        exit()
# flatten list of lists of specific dirs
args.extra = list(it.chain(*[s.split(',') for s in args.extra]))
if args.serial:
    RECURSE = set()

print(args)
if __name__ == "__main__":
    with cd(args.build_dir):
        trio.run(main, args)
