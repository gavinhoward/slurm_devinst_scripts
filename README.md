# slurm_devinst_scripts
# Instance layout
```
slurm - Instance top directory
    slurmbase - repo
    slurm_master - instance dir
        .envrc
	compile_commands.json - optional, created by bear and make_compiledb
        caesar - cluster name
	    bin
	    build
	    etc
	    sbin
	    ...
	source
    slurm_1808 - another instance dir
        ...
```
All functions in functions.sh are valid when run inside an instance dir. They
read `$SLURM_PREFIX`, which is added to the environment by `direnv` reading `.envrc`.

# Files
#### direnv
Install `direnv` and enable for your shell

#### envrc
This is a template file that is copied into the instance and filled with
instance-specific environment variables. `direnv` loads this file, renamed to
`.envrc`.

#### functions.sh
Source `functions.sh` in shell rc

#### mkslurm.sh
Shell script to make the instance. Handles copying template files into instance
and editing the templates. Expects and checks several environment variables.
```
USER=		# shell standard, current user
CLUSTER=	# Name of the cluster, usually the machine's hostname
INST_ROOT=	# Path to directory containing all instances
TEMPLATE_DIR=	# Path to this repo containing template files for new instances
SLURM_GIT=	# Path to full slurm git mirror. Currently assumes it is local
```

#### make.py
Full-featured make wrapper. Run `make.py --init` from within the instance to
configure and build the first time.

#### etc
Template files for <instance>/<cluster>/etc. These are modified by mkslurm.sh
for each instance.

#### slurm-update
Contains the script, service, and timer files for keeping a local mirror of
slurm.git up to date. They will need to be modified. Change the `User` in
`slurm-update.service`, and change the paths in all of them to match your needs.

# PATH
Link mkslurm.sh and make.py into your path. I did:
```
~/bin/mkslurminst -> <repo>/mkslurm.sh
~/bin/makepy -> <repo>/make.py
```

# functions.sh
```
check_env - tests for SLURM_PREFIX, used by other functions
restart_slurmd - restarts all node slurmd's //TODO parameterize
restart_slurm - restarts all daemons, including slurmd's
clear_slurmdb - drops default instance databases
init_slurmdb - Add cluster, accounts, and users to accounting
pidstop - stops a list of pids. Used internally.
stop_slurmd - Stops all slurmd's
stop_slurmdbd - Stops slurmdbd
stop_slurmctld - Stops slurmctld
stop_slurm - Stops all daemons, via other stop functions
kill_slurm - Force kills all daemons with pids in $SLURM_PREFIX/run
make_compiledb - Makes compile flag database for completion tools, requires bear
```

# make.py
```
❯ makepy -h
usage: makepy [-h] [--with-docs] [--with-contribs] [--with-all] [--no-install]
              [--with-recheck] [--with-reconfig] [--with-full-reconfig]
              [--with-clean] [--serial] [--stdout STDOUT] [--stderr STDERR]
              [-v] [-q] [--extra EXTRA] [--init]
              [build_dir]

Fast make slurm. Does not make docs or contribs by default.

positional arguments:
  build_dir             specify the build directory. Current working directory
                        is used by default.

optional arguments:
  -h, --help            show this help message and exit
  --with-docs           also make docs
  --with-contribs       also make contribs
  --with-all            make docs and contribs
  --no-install          build only, do not install
  --with-recheck        run config.status --recheck before make
  --with-reconfig       run config.status before make
  --with-full-reconfig  run config.status and config.status --recheck before
                        build
  --with-clean          run make clean before make
  --serial, -s          Disable parallel build, just run make from the top
  --stdout STDOUT       specify a file to write make stdout to
  --stderr STDERR       specify a file to write make stderr to
  -v, --verbose         verbose output
  -q, --quiet           quiet output
  --extra EXTRA         specify extra subdirs to build, such as contribs/pmi2.
                        Use multiple times or pass comma-separated list
  --init                initialize build dir using configure flags in
                        CONFIG_FLAGS. Also enables --serial
```
